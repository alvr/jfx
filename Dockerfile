FROM openjdk:8-jdk

RUN apt-get update \
    && apt-get install --no-install-recommends -y \
        openjfx \
        unzip \
		gradle \
    && apt-get clean \
    && rm -f /var/lib/apt/lists/*_dists_*